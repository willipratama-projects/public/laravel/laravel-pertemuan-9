<?php

namespace App\Http\Controllers;

use App\Portfolio;
use Illuminate\Http\Request;

class LandingController extends Controller
{
    public function index()
    {
        return view('contents.landing.index');
    }

    public function portfolios()
    {
        $portfolios = Portfolio::all();
        return view('contents.landing.portfolios.index', compact('portfolios'));
    }

    public function about()
    {
        return view('contents.landing.about');
    }
}
