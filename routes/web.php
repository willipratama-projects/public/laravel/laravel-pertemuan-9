<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'LandingController@index')->name('index');

Route::get('/portfolios', 'LandingController@portfolios')->name('portfolios');
Route::get('/portfolio', function () {
    return redirect()->route('portfolios');
});
Route::get('/portfolio/{portfolio}', 'PortfolioController@show')->name('portfolio.detail');

Route::get('/about', 'LandingController@about')->name('about');

Auth::routes([
    //
]);

Route::get('/dashboard', 'DashboardController@index')->name('dashboard');
